## [6.8.11] - 2024-08-21
### Updated
- Rollback specific for Bedrock to a previous version of the adapter.
### Library
- Packaged with `lib 6.8.57`

## [6.8.10] - 2024-05-21
### Updated
- Fix issue when firing the /join event when buffering at the beginning.
### Library
- Packaged with `lib 6.8.56`

## [6.8.9] - 2023-03-24
### Library
- Packaged with `lib 6.8.46`

## [6.8.8] - 2023-03-22
### Library
- Packaged with `lib 6.8.45`

## [6.8.6] - 2022-10-31
### Added
- Improve join time calculation if receive a playing event while buffering
- Adding the buffer time after seek into the same seek event (to avoid false buffer after seek events)

## [6.8.5] - 2022-10-26
### Added
- Report triggered events parameter for the fired plugin events
- Using buffer events instead of waiting from video player, and disable buffer monitor 
### Library
- Packaged with `lib 6.8.33`

## [6.8.4] - 2022-07-18
### Library
- Packaged with `lib 6.8.26`

## [6.8.3] - 2022-05-24
### Fixed
- Fake view created between content loads when `loadstart` is triggered

## [6.8.2] - 2022-03-29
### Added
- `getLatency` implementation to report live video latency for shaka 3.0+
### Library
- Packaged with `lib 6.8.15`

## [6.8.1] - 2021-08-20
### Added
- Listener for `loadstart` event to split views when loading new content
### Library
- Packaged with `lib 6.8.1`

## [6.8.0] - 2021-08-09
### Added
- Listener for waiting event to support `content.isLive.noMonitor` functionality fallback
### Library
- Packaged with `lib 6.8.0`

## [6.7.13] - 2021-07-30
### Fixed
- Issues with start detection for Vod videos, if seeking back to the beggining when ending the content

## [6.7.12] - 2021-07-30
### Fixed
- Issues with seek end detection
### Library
- Packaged with `lib 6.7.41`

## [6.7.11] - 2021-07-19
### Fixed
- Timeupdate event detection before player starts to reproduce content
### Library
- Packaged with `lib 6.7.40`

## [6.7.10] - 2021-06-14
### Fixed
- Tag reference when unregistering listeners
### Library
- Packaged with `lib 6.7.39`

## [6.7.9] - 2021-05-04
### Fixed
- Removed es6 syntax on getTagFromPlayer definition
### Library
- Packaged with `lib 6.7.33`

## [6.7.8] - 2021-04-19
### Added
- More control for start/join events timing for live videos on different player versions
### Library
- Packaged with `lib 6.7.31`

## [6.7.7] - 2021-02-12
### Added
- Second optional parameter for the adapter constructor to send a reference of the video element.
### Library
- Packaged with `lib 6.7.27`

## [6.7.6] - 2021-01-20
### Fixed
- Error metadata detection for v1 v2 and v3 of the player
### Added
- Refactor of the `this.tag` variable assign

## [6.7.5] - 2021-01-12
### Fixed
- Issues getting the playrate with shaka versions 1.3 or lower
- Error message not being reported for shaka player versions 1.3 or lower
### Library
- Packaged with `lib 6.7.25`

## [6.7.4] - 2020-12-03
### Library
- Packaged with `lib 6.7.24`

## [6.7.3] - 2020-10-19
### Fixed
- Unsafe access to global variable `shaka`
### Library
- Packaged with `lib 6.7.17`

## [6.7.2] - 2020-05-29
### Library
- Packaged with `lib 6.7.7`

## [6.7.1] - 2020-04-23
### Fixed
- Checked for all playhead changes to trigger jointime event
### Library
- Packaged with `lib 6.7.5`

## [6.7.0] - 2020-03-20
### Fixed
- Support for shaka 2.5.x and higher versions, not having reference to video tag from the beggining
### Library
- Packaged with `lib 6.7.0`

## [6.5.4] - 2020-02-04
### Fixed
- Sometimes buffer being sent after seek
### Added
- Small general refactor
### Library
- Packaged with `lib 6.5.25`

## [6.5.3] - 2019-11-04
### Fixed
- Playhead additional check for videos not starting at playhead 0 but reporting jointime with timeupdate event
### Library
- Packaged with `lib 6.5.20`

## [6.5.2] - 2019-10-04
### Fixed
- Error listeners
### Library
- Packaged with `lib 6.5.16`

## [6.5.1] - 2019-08-12
### Added
- Listener for `timeupdate` to trigger jointime in some cases
### Library
- Packaged with `lib 6.5.11`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.4.10] - 2019-03-26
### Fixed
- Error 3016 changed to nonfatal

## [6.4.9] - 2019-03-21
### Fixed
- Error 1002 changed to nonfatal
- Fixed seek reported instead of buffer after network restriction

## [6.4.8] - 2019-03-18
### Fixed
- Undefined monitor check

## [6.4.7] - 2019-02-19
### Added
- Error listener for the tag removed, not needed anymore
### Fixed
- Error severity detection
### Library
- Packaged with `lib 6.4.16`

## [6.4.6] - 2019-02-06
### Added
- Error listener for the player, not the tag
### Library
- Packaged with `lib 6.4.15`

## [6.4.5] - 2018-12-20
### Added
- New method to get resource
### Library
- Packaged with `lib 6.4.12`

## [6.4.4] - 2018-08-29
### Library
- Packaged with `lib 6.4.5`

## [6.4.3] - 2018-08-28
### Library
- Packaged with `lib 6.4.4`

## [6.4.2] - 2018-08-24
### Fix
- Fixed playrate for live content being reported as 0

## [6.4.1] - 2018-08-22
### Fix
- Fixed source getter
- Fixed isLive getter

## [6.4.0] - 2018-08-17
### Library
- Packaged with `lib 6.4.1`

## [6.3.0] - 2018-07-10
### Library
- Packaged with `lib 6.3.2`
### Added
- Support for shaka 1.x

## [6.2.0] - 2018-04-09
### Library
- Packaged with `lib 6.2.0`

## [6.1.0] - 2018-02-26
### Library
- Packaged with `lib 6.1.12`
